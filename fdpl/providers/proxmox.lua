local https = require('ssl.https')
local ltn12 = require('ltn12')
local url = require('socket.url')
local json = require('cjson')

local Proxmox = {}
Proxmox.__index = Proxmox

function Proxmox.new(opts)
	local self = setmetatable(opts, Proxmox)

	self.ticket = self:authenticate()

	return self
end

function Proxmox.authenticate(self)
	local post_data = 'username='
		.. self.config.api_username
		.. '&password='
		.. url.escape(self.config.api_password)

	local chunks = {}
	local body, code, headers, status = https.request({
		method = 'POST',
		url = self.config.api_url .. '/access/ticket',
		headers = {
			['Content-Type'] = 'application/x-www-form-urlencoded',
			['Content-Length'] = #post_data,
		},
		source = ltn12.source.string(post_data),
		sink = ltn12.sink.table(chunks),
	})

	if #chunks == 0 or code ~= 200 then
		error('Failed to authenticate with proxmox: ' .. status .. ' ')
	end

	return json.decode(chunks[1]).data
end

function Proxmox.commit(self)
	for _, action in pairs(self.config.actions) do
		print('running action ' .. action.action .. ' on ' .. action.kind .. '...')
	end
end

return Proxmox
