local providers = require('providers')

for _, providerConfig in pairs(providers) do
	local Provider = require('fdpl.providers.' .. providerConfig.kind)
	local provider = Provider.new({
		config = providerConfig,
	})

	provider:commit()
end
